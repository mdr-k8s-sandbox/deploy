#!/bin/sh

echo "|\---/|"
echo "| o_o |"
echo " \_^_/"
echo Create namespace
kubectl create -f namespace.yaml

echo Deploy mongodb...
kubectl create -f db/

echo Deploy redis...
kubectl create -f redis/

echo Deploy rabbitmq...
kubectl create -f rabbitmq/

echo Wait 30s...
sleep 30

kubectl get deploy,pod,svc,ingress -n api

echo Deploy trending scraper...
kubectl create -f trending-scraper/

echo Deploy worker...
kubectl create -f worker/

echo Deploy scraper API...
kubectl create -f scraper-api/

echo Deploy scraper UI...
kubectl create -f scraper-ui/

echo Create ingress...
kubectl create -f ingress.yaml