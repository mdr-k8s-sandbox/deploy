# MDR K8S deployment project

## Deploy stack

```sh
sh run.sh
```

## Diagram

```mermaid
graph LR
    id1[[Scraper UI - VueJS]] -->|HTTP| id2[Scraper API - NodeJS]
    id2 --> id4[(Mongodb)]
	id2 -->|gRPC| id3[Trending scraper - Go]
    id3 .-> id2
    id2 -->|cache| id5[(redis)]
    id2 -->|Pub| id6([RabbitMQ])
    id7[Worker PDF - NodeJS] -->|Sub|id6
```
